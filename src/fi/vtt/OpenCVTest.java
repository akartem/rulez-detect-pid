package fi.vtt;

import java.io.File;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

//
// Detects faces in an image, draws boxes around them, and writes the results
// to "faceDetection.png".
//
class DetectFaceDemo {
  public void run() {
    System.out.println("\nRunning DetectFaceDemo");

    //Create a face detector from the cascade file
    //CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("lbpcascade_frontalface.xml").getPath().substring(1));

    System.out.println(new File("lbpcascade_frontalface.xml").getAbsolutePath());

    CascadeClassifier faceDetector = new CascadeClassifier(new File("lbpcascade_frontalface.xml").getAbsolutePath());

    Mat image = Imgcodecs.imread(new File("lena.png").getAbsolutePath());
    // Detect faces in the image.
    // MatOfRect is a special container class for Rect.
    MatOfRect faceDetections = new MatOfRect();
    
    faceDetector.detectMultiScale(image, faceDetections);
    
    System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));
    
    // Draw a bounding box around each face.
    for (Rect rect : faceDetections.toArray()) {
        Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
    }

    // Save the visualized detection.
    String filename = "faceDetection.png";
    System.out.println(String.format("Writing %s", filename));
    Imgcodecs.imwrite(filename, image);
    System.out.println("done");
  }
}

//Detects lines
class DetectLines {
	public void run() {
		Mat image = Imgcodecs.imread(new File("tank_pump_turned.png").getAbsolutePath());
		Mat edges = new Mat();
		
		Imgproc.cvtColor(image, edges, Imgproc.COLOR_RGB2GRAY);

		Imgproc.blur(edges, edges, new Size(3,3));
		
		Imgproc.Canny(edges, edges, 50, 200); 
		
		Mat lines = getHoughPTransform(edges);
		
	    System.out.println(String.format("Detected %s lines", lines.rows()));

	    for (int i = 0; i < lines.rows(); i++) {
		    for (int j = 0; j < lines.cols(); j++) {
		        double[] val = lines.get(i, j);
		        Imgproc.line(image, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 2);
		    }
	    }


		String efilename = "edges.png";
	    System.out.println(String.format("Writing %s", efilename));
	    Imgcodecs.imwrite(efilename, edges);

	    String filename = "lines.png";
	    System.out.println(String.format("Writing %s", filename));
	    Imgcodecs.imwrite(filename, image);
	    System.out.println("done");
		
	}
	  
	public Mat getHoughPTransform(Mat image) {
	    //Mat result = Image.getInstance().getImage().clone();
	    Mat lines = new Mat();
	    
	    double rho = 1; // The resolution of the parameter rho in pixels
	    double theta = Math.PI/720; //The resolution of the parameter theta in radians
	    int threshold = 50; //The minimum number of "intersections" to �detect� a line (sensitivity?)
	    
	    double minLineLength = 50; //The minimum number of points that can form a line. Lines with less than this number of points are disregarded.
	    double maxLineGap = 10; //The maximum gap between two points to be considered in the same line.
	    
	    Imgproc.HoughLinesP(image, lines, rho, theta, threshold, minLineLength, maxLineGap);

	    return lines;
	}
}


public class OpenCVTest {
  public static void main(String[] args) {
    // Load the native library.
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    
    //new DetectFaceDemo().run();
    
    new DetectLines().run();
  }
}

//public class Hello
//{
//   public static void main( String[] args )
//   {
//      System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
//      Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
//      System.out.println( "mat = " + mat.dump() );
//   }
//}
